import os
import gdown
from zipfile import ZipFile

script_dir = os.path.dirname(os.path.realpath(__file__))

dataset_url = 'https://drive.google.com/uc?id=1XGN0HQKar-ApXrn0XljYQxTnbIP5sjfz'
models_url = 'https://drive.google.com/uc?id=1mE0hoAmqwJxgfEWkoJOmOv8q3MqP3La3'
zip_dataset_file = 'dataset.zip'
zip_models_file = 'models.zip'
dataset_md5 = '84a53c17a9e73f2fce57e7d8d5abd946'
models_md5 = '4a1095b81aa286fe861c5d40b35b4b3c'

gdown.cached_download(dataset_url, zip_dataset_file, md5=dataset_md5)
gdown.cached_download(models_url, zip_models_file, md5=models_md5)

with ZipFile(zip_dataset_file, 'r') as zipObj:
   # Extract all the contents of zip file in current directory
   zipObj.extractall(path=script_dir+'/app/src/main/assets/')
   
with ZipFile(zip_models_file, 'r') as zipObj:
   # Extract all the contents of zip file in current directory
   zipObj.extractall(path=script_dir+'/app/src/main/assets/')

os.remove(zip_dataset_file)
os.remove(zip_models_file)

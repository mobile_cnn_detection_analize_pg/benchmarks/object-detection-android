# Android Object Detection Benchmark

## Build

```
python3 setup.py
./gradlew build
```

## Install Apk

```
adb install  -t ./app/build/outputs/apk/release/app-release-unsigned.apk
```
Application should appear in your app menu

## Retrive results from the device

```
./retrive-results.sh
```

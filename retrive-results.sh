adb pull /storage/emulated/0/Android/data/pl.edu.pg.inz2021.objectdetection/files/results/
for device_path in ./results/*
do
device_dir=$(basename $device_path)
cd ./results/
zip -r $device_dir.zip $device_dir
cd -
rm -r $device_path
done

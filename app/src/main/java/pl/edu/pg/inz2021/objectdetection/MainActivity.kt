/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.edu.pg.inz2021.objectdetection

import android.os.Bundle
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "TFLite - ODT"
    }

    private lateinit var benchmark: Benchmark
    private lateinit var benchmarkThread: Thread
    lateinit var benchmarkProgressBar: ProgressBar
    lateinit var stageProgressBar: ProgressBar
    lateinit var progressMessage: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        benchmarkProgressBar = findViewById(R.id.benchmarkProgressBar)
        stageProgressBar = findViewById(R.id.stageProgressBar)
        progressMessage = findViewById(R.id.progressMessage)

        benchmark = Benchmark(this, benchmarkProgressBar, stageProgressBar, progressMessage)
        benchmarkThread = Thread(Runnable { runBenchmark() })
        benchmarkThread.start()
    }

    fun runBenchmark() {
        benchmark.run()
        runOnUiThread(Runnable {
            val notice = findViewById<TextView>(R.id.tvPlaceholder)
            notice.setText(R.string.notification_finished)
        })
    }
}
